# Logstash and logstash-forwarder configuration files 

This repository contains a set of files to faciliate the configuration of logstash and logstash-forwarder. 
A formal installation process has yet to be defined. 
These files are currently serving to augment the installation instructions at:
https://cccnext.jira.com/wiki/display/DEVOPS/ELK+Stack+setup+steps

# logstash server files

files in logstash-server/conf.d should be added to the logstash server instance at /etc/logstash/conf.d 


# logstash-forwarder setup

    wget https://download.elastic.co/logstash-forwarder/binaries/logstash-forwarder_0.4.0_amd64.deb
    sudo dpkg -i logstash-forwarder_0.4.0_amd64.deb

## copy the server certificate onto the client:

    sudo mkdir -p /etc/pki/tls/certs
    sudo cp logstash-forwarder/certs/logstash-forwarder.crt /etc/pki/tls/certs/  

## copy logstash-forwarder.conf on the client
    sudo cp logstash-forwarder/logstash-forwarder.conf /etc/logstash-forwarder.conf
    